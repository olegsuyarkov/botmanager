FROM python:3.8.1

RUN apt-get install -y git && \
    pip3 install pipenv && \
    git config --global user.name "Oleg Suyarkov" && \
    git config --global user.email olegsuyarkov@gmail.com

WORKDIR /usr/src/bot_manager

COPY Pipfile .
COPY Pipfile.lock .

RUN pipenv sync