import asyncio
import sys
import os
from aio_pika import connect, ExchangeType
from bots.general_task_router import on_message_task_router


async def main(loop):
    # Perform connection
    connection = await connect(
        "amqp://{}:{}@rabbitmq/".format(os.getenv('RABBITMQ_DEFAULT_USER'), os.getenv('RABBITMQ_DEFAULT_PASS')), loop=loop
    )

    # Creating a channel
    channel = await connection.channel()
    await channel.set_qos(prefetch_count=1)

    # Declare an exchange
    exchange_name = await channel.declare_exchange(
        sys.argv[1], ExchangeType.TOPIC
    )

    # Declaring queue
    queue = await channel.declare_queue(
        sys.argv[1], durable=True
    )

    binding_keys = sys.argv[2:]

    if not binding_keys:
        sys.stderr.write(
            "Usage: %s [binding_key]...\n" % sys.argv[0]
        )
        sys.exit(1)

    for binding_key in binding_keys:
        await queue.bind(exchange_name, routing_key=binding_key)

    # Start listening the queue
    await queue.consume(on_message_task_router)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.create_task(main(loop))

    # we enter a never-ending loop that waits for
    # data and runs callbacks whenever necessary.
    print(" [*] Waiting for messages. To exit press CTRL+C")
    loop.run_forever()