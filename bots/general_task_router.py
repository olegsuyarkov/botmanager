import os
import motor.motor_asyncio
from aiohttp import ClientSession
from aio_pika import IncomingMessage
from .masterbot import tasks

get_task = {
    'masterbot': {
        'webhook': tasks.webhook
    },
    'cargobot': {}
}

tools = {}

# init http session
tools['http'] = ClientSession()

# init mongo
connection_string = 'mongodb://{}:{}@mongodb:27017'.format(os.getenv('MONGO_INITDB_ROOT_USERNAME'), os.getenv('MONGO_INITDB_ROOT_PASSWORD'))
tools['motor'] = motor.motor_asyncio.AsyncIOMotorClient(connection_string)
tools['db_masterbot'] = tools['motor'].masterbot


async def on_message_task_router(message: IncomingMessage):
    # async with message.process(requeue=True):
    async with message.process():
        routing = message.routing_key.split('.')
        task_func = routing[0]
        action = routing[1]
        message_info = message.info()
        await get_task[message_info['exchange']][task_func](action, message.body, tools)