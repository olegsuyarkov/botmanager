import os


BOT_NAME = 'MasterBot'
BOT_URI_ID = 'master_bot'
BOT_TOKEN = os.getenv('MASTERBOT_TOKEN')
DEFAULT_LANGUAGE = 'ru'