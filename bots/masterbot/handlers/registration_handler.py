from ..sender import send_message
from ..lang_pack import langmes
from ...general_constants import VIBER_VIOLET, VIBER_WHITE, SECRET_KEY


# handler by text command "Start"
async def command_start(data, app, track_data):
    if 'conversation' in track_data and track_data['conversation']=='yes':
        message = {
            'receiver': data['sender']['id'],
            'text': langmes(data['sender']['language'], 'first message'),
            'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'BgColor' : VIBER_VIOLET,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'send number btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'share-phone',
                            'ActionBody' : SECRET_KEY + 'register number'
                        }
                    ]
                }
        }
        await send_message(app, message)


# registration phone number of user
async def register_number(data, app, track_data):
    db = app['db_masterbot']
    await db.users.update_one({'viber_id': data['sender']['id']}, {'$set':{'name': data['sender']['name'], 'language': data['sender']['language'], 'country': data['sender']['country'], 'phone': data['message']['contact']['phone_number'], 'status': 'subscribed'}}, upsert=True)
    message = {
            'receiver': data['sender']['id'],
            'text': langmes(data['sender']['language'], 'register number already'),
        }
    await send_message(app, message)