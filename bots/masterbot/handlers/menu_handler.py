from ..sender import send_message
from ..lang_pack import langmes
from ...general_constants import VIBER_BLUE, VIBER_WHITE, VIBER_RED, SECRET_KEY


# handler by text command "Menu"
async def command_menu(data, app, track_data):
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'menu message'),
        'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_BLUE,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'my bots btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : SECRET_KEY + 'my bots btn'
                        },
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_BLUE,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'payment services btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : SECRET_KEY + 'payment services btn'
                        },
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_RED,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'support btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : SECRET_KEY + 'support btn'
                        }
                    ]
                }
    }
    await send_message(app, message)