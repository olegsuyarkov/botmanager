import json
from aio_pika import Message, DeliveryMode
from ..sender import send_message
from ..lang_pack import langmes, langexist
from ...general_constants import VIBER_YELLOW, VIBER_BLUE, VIBER_WHITE, VIBER_VIOLET, VIBER_RED, VIBER_DARK_GREY, VIBER_PARTNERS_URL, SECRET_KEY


# handler by item "My bots" in menu
async def my_bots_btn(data, app, track_data):
    db = app['db_masterbot']
    bots = await db.users.find_one({'viber_id' : data['sender']['id']}, {'_id' : 0, 'bots' : 1})
    buttons = [
                {
                    'Columns': 6,
                    'Rows': 1,
                    'BgColor' : VIBER_VIOLET,
                    'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'create bot btn')),
                    'TextSize' : 'large',
                    'ActionType' : 'reply',
                    'ActionBody' : SECRET_KEY + 'create bot btn'
                },
                {
                    'Columns': 6,
                    'Rows': 1,
                    'BgColor' : VIBER_YELLOW,
                    'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'btn-back')),
                    'TextSize' : 'large',
                    'ActionType' : 'reply',
                    'ActionBody' : 'btn_back'
                }
            ]

    if 'bots' not in bots:
        text = langmes(data['sender']['language'], 'not any bots')
    else:
        name_counter = 0
        for bot in bots['bots']:
            if 'bot_name' in bot:
                name_counter +=1
        if name_counter == 0:
            text = langmes(data['sender']['language'], 'not any bots')
        else:
            text = langmes(data['sender']['language'], 'actions with bots')
            button = {
                        'Columns': 6,
                        'Rows': 1,
                        'BgColor' : VIBER_BLUE,
                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'actions with bots btn')),
                        'TextSize' : 'large',
                        'ActionType' : 'reply',
                        'ActionBody' : SECRET_KEY + 'bots actions'
                    }
            buttons.insert(0, button)
        
    message = {
            'receiver' : data['sender']['id'],
            'text' : text,
            'keyboard': {
                'DefaultHeight' : False,
                'Buttons' : buttons
            }
        }
    track_data = {
        'prev': 'Menu'
    }
    await send_message(app, message, track_data)


# handler create bot btn
async def create_bot_btn(data, app, track_data):
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'create new bot'),
        'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_YELLOW,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'btn-next')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : SECRET_KEY + 'choose bot template'
                        }
                    ]
                }
    }
    await send_message(app, message)


# choose bot template handler
async def choose_bot_template(data, app, track_data):
    db = app['db_masterbot']
    bots_templates = db.bots_templates.find()
    buttons = []
    for bot_template in await bots_templates.to_list(length=200):
        button = {
                'Columns': 6,
                'Rows': 2,
                'BgColor' : VIBER_WHITE,
                'Text' : '<font color="{}"><b>{}</b><br>{}</font>'.format(VIBER_DARK_GREY, bot_template['options'][langexist(data['sender']['language'])]['system_name'], bot_template['options'][langexist(data['sender']['language'])]['description']),
                'TextSize' : 'large',
                'ActionType' : 'reply',
                'ActionBody' : SECRET_KEY + '{} bot type'.format(bot_template['name'])
            }
        buttons.append(button)

    message = {
            'receiver' : data['sender']['id'],
            'text' : langmes(data['sender']['language'], 'list of bots tpl'),
            'keyboard': {
                'DefaultHeight' : False,
                'Buttons' : buttons
            }
        }
    await send_message(app, message)


# cargobot type bot handler
# response partners url and button next
async def cargobot_type_bot(data, app, track_data):
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'partners viber') + ' ' + VIBER_PARTNERS_URL,
        'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_YELLOW,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'btn-next')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : SECRET_KEY + 'get uri bot'
                        }
                    ]
                }
    }
    track_data = {
        'bot_type': 'cargobot'
    }
    await send_message(app, message, track_data)


# get uri bot handler
async def get_uri_bot(data, app, track_data):
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'copy bot uri')
    }
    track_data.update({'current': 'get token bot'})
    await send_message(app, message, track_data)


# get token bot handler
async def get_token_bot(data, app, track_data):
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'copy bot token')
    }
    track_data.update({'bot_uri': data['message']['text'], 'current': 'save bot info'})
    await send_message(app, message, track_data)


# save all info about bot to db and create task to webhook
async def save_bot_info(data, app, track_data):
    track_data.pop('current', None)
    track_data.update({'viber_id': data['sender']['id'], 'lang': data['sender']['language'], 'bot_token': data['message']['text']})
    message_body = Message(json.dumps(track_data).encode(), delivery_mode=DeliveryMode.PERSISTENT)
    await app['masterbot_exchange'].publish(message_body, routing_key='webhook.create')
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'save bot info'),
        'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_YELLOW,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'btn-menu')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : 'Menu'
                        }
                    ]
                }
    }
    await send_message(app, message)


# actions with existing user's bots
async def bots_actions(data, app, track_data):
    db = app['db_masterbot']
    bots = await db.users.find_one({'viber_id': data['sender']['id']}, {'_id' : 0, 'bots' : 1})
    buttons = []
    for bot in bots['bots']:
        actionbody = {
            'bot_uri': bot['bot_uri'],
            'bot_type': bot['bot_type'],
            'bot_token': bot['bot_token']
        }
        actionbody = json.dumps(actionbody)
        button = {
            'Columns': 6,
            'Rows': 1,
            'BgColor' : VIBER_WHITE,
            'Text' : '<font color="{}"><b>{}</b><br>{}</font>'.format(VIBER_DARK_GREY, bot['bot_name'], bot['system_name']),
            'TextSize' : 'large',
            'ActionType' : 'reply',
            'ActionBody' : actionbody
        }
        buttons.append(button)
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'choose your bot'),
        'keyboard': {
                'DefaultHeight' : False,
                'Buttons' : buttons
            }
    }
    track_data = {
        'current': 'actions selected bot'
    }
    await send_message(app, message, track_data)


# actions with selected bot
async def actions_selected_bot(data, app, track_data):
    try:
        text = json.loads(data['message']['text'])
        db = app['db_masterbot']
        bot_type = await db.bots_templates.find_one({'name': text['bot_type']})
        buttons = []
        for action in bot_type['actions']:
            button = {
                    'Columns': 6,
                    'Rows': 1,
                    'BgColor' : VIBER_BLUE,
                    'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, bot_type['options'][langexist(data['sender']['language'])][action]),
                    'TextSize' : 'large',
                    'ActionType' : 'reply',
                    'ActionBody' : SECRET_KEY + action
                }
            buttons.append(button)
        message = {
            'receiver': data['sender']['id'],
            'text': langmes(data['sender']['language'], 'choose action bot'),
            'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : buttons
                }
        }
        track_data = {
            'bot_type': text['bot_type'],
            'bot_uri': text['bot_uri'],
            'bot_token': text['bot_token']
        }
        await send_message(app, message, track_data)
    except json.JSONDecodeError:
        pass


# on/off bot, choose variant
async def on_off_bot(data, app, track_data):
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'active or deactive bot'),
        'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_VIOLET,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'on bot btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : 'activated'
                        },
                        {
                            'Columns': 6,
		                    'Rows': 1,
                            'BgColor' : VIBER_RED,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'off bot btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : 'deactivated'
                        }
                    ]
                }
    }
    track_data.update({'current': 'on/off bot action'})
    await send_message(app, message, track_data)


# on/off bot action with chosen variant
async def on_off_bot_action(data, app, track_data):
    db = app['db_masterbot']
    if data['message']['text'] == 'activated' or data['message']['text'] == 'deactivated':
        message_body = {
            'viber_id': data['sender']['id'],
            'bot_token': track_data['bot_token']
        }
        routing_key = 'webhook.{}'.format(data['message']['text'])
        message_body = Message(json.dumps(message_body).encode(), delivery_mode=DeliveryMode.PERSISTENT)
        await app['masterbot_exchange'].publish(message_body, routing_key=routing_key)
        await db.users.update_one({'viber_id': data['sender']['id'], 'bots.bot_uri': track_data['bot_uri']}, {'$set': {'bots.$.status': data['message']['text']}})
        message = {
            'receiver': data['sender']['id'],
            'text': langmes(data['sender']['language'], 'bot will be'),
            'keyboard': {
                        'DefaultHeight' : False,
                        'Buttons' : [
                            {
                                'Columns': 6,
                                'Rows': 1,
                                'BgColor' : VIBER_YELLOW,
                                'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'back to my bots btn')),
                                'TextSize' : 'large',
                                'ActionType' : 'reply',
                                'ActionBody' : SECRET_KEY + 'my bots btn'
                            }
                        ]
                    }
        }
        await send_message(app, message)


# add/del user bot choose list of users
async def add_del_user_bot(data, app, track_data):
    track_data.update({'current': 'add/del user bot'})
    db = app['db_masterbot']
    if data['message']['type'] == 'contact':
        if 'contacts' in track_data:
            track_data['contacts'].append({'name': data['message']['contact']['name'], 'phone': data['message']['contact']['phone_number']})
        else:
            track_data.update({'contacts': [{'name': data['message']['contact']['name'], 'phone': data['message']['contact']['phone_number']}]})
        count_of_users = len(track_data['contacts'])
        message = {
                'receiver': data['sender']['id'],
                'text': langmes(data['sender']['language'], 'users selected') + ' ' + str(count_of_users) + '\n' + langmes(data['sender']['language'], 'send user phone again'),
                'keyboard': {
                            'DefaultHeight' : False,
                            'Buttons' : [
                                {
                                    'Columns': 6,
                                    'Rows': 1,
                                    'BgColor' : VIBER_BLUE,
                                    'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'add user btn')),
                                    'TextSize' : 'large',
                                    'ActionType' : 'reply',
                                    'ActionBody' : 'add user btn'
                                },
                                {
                                    'Columns': 6,
                                    'Rows': 1,
                                    'BgColor' : VIBER_RED,
                                    'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'del user btn')),
                                    'TextSize' : 'large',
                                    'ActionType' : 'reply',
                                    'ActionBody' : 'del user btn'
                                }
                            ]
                        }
            }
        
    elif data['message']['type'] == 'text':
        if 'contacts' not in track_data:
            message = {
                'receiver': data['sender']['id'],
                'text': langmes(data['sender']['language'], 'send user phone')
                }
        elif data['message']['text'] == 'add user btn':
            await db.users.update_one({'viber_id': data['sender']['id'], 'bots.bot_uri': track_data['bot_uri']}, {'$addToSet': {'bots.$.contacts': {'$each': track_data['contacts']}}})
            message = {
                    'receiver': data['sender']['id'],
                    'text': langmes(data['sender']['language'], 'users added'),
                    'keyboard': {
                                'DefaultHeight' : False,
                                'Buttons' : [
                                    {
                                        'Columns': 6,
                                        'Rows': 1,
                                        'BgColor' : VIBER_YELLOW,
                                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'back to my bots btn')),
                                        'TextSize' : 'large',
                                        'ActionType' : 'reply',
                                        'ActionBody' : SECRET_KEY + 'my bots btn'
                                    }
                                ]
                            }
                }
            track_data.pop('current', None)
        elif data['message']['text'] == 'del user btn':
            await db.users.update_one({'viber_id': data['sender']['id'], 'bots.bot_uri': track_data['bot_uri']}, {'$pull': {'bots.$.contacts': {'$in': track_data['contacts']}}})
            message = {
                    'receiver': data['sender']['id'],
                    'text': langmes(data['sender']['language'], 'users delete'),
                    'keyboard': {
                                'DefaultHeight' : False,
                                'Buttons' : [
                                    {
                                        'Columns': 6,
                                        'Rows': 1,
                                        'BgColor' : VIBER_YELLOW,
                                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'back to my bots btn')),
                                        'TextSize' : 'large',
                                        'ActionType' : 'reply',
                                        'ActionBody' : SECRET_KEY + 'my bots btn'
                                    }
                                ]
                            }
                }
            track_data.pop('current', None)
    # ТУТ ЕЩЕ ПОДОБАВЛЯТЬ ЗАДАНИЯ В РЭДИС ДЛЯ ДОБАВЛЕНИЯ И УДАЛЕНИЯ ПОЛЬЗОВАТЕЛЕЙ
    if 'message' in locals():
        await send_message(app, message, track_data)


# get list of users bot
async def list_of_users_bot(data, app, track_data):
    db = app['db_masterbot']
    bots = await db.users.find_one({'viber_id': data['sender']['id']}, {'_id' : 0, 'bots' : 1})
    list_of_users = ''
    for bot in bots['bots']:
        if bot['bot_uri'] == track_data['bot_uri']:
            if 'contacts' in bot:
                for contact in bot['contacts']:
                    list_of_users += '\n\n {} \n {}'.format(contact['name'], contact['phone'])
            break
    if list_of_users == '':
        message = {
                'receiver': data['sender']['id'],
                'text': langmes(data['sender']['language'], 'empty list of users'),
                'keyboard': {
                            'DefaultHeight' : False,
                            'Buttons' : [
                                {
                                    'Columns': 6,
                                    'Rows': 1,
                                    'BgColor' : VIBER_BLUE,
                                    'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'add new user btn')),
                                    'TextSize' : 'large',
                                    'ActionType' : 'reply',
                                    'ActionBody' : SECRET_KEY + 'add/del user bot'
                                }
                            ]
                        }
            }
    else:
        message = {
                'receiver': data['sender']['id'],
                'text': langmes(data['sender']['language'], 'list of added users') + list_of_users,
                'keyboard': {
                            'DefaultHeight' : False,
                            'Buttons' : [
                                {
                                    'Columns': 6,
                                    'Rows': 1,
                                    'BgColor' : VIBER_BLUE,
                                    'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'add/del user btn')),
                                    'TextSize' : 'large',
                                    'ActionType' : 'reply',
                                    'ActionBody' : SECRET_KEY + 'add/del user bot'
                                }
                            ]
                        }
            }
    await send_message(app, message, track_data)


# add/del site using in bot
async def add_del_site_bot(data, app, track_data):
    db = app['db_masterbot']
    bot_type = await db.bots_templates.find_one({'name': track_data['bot_type']}, {'_id' : 0, 'actions_options' : 1})
    buttons = []
    for action_option in bot_type['actions_options']['add/del site bot']:
        button = {
                    'Columns': 6,
                    'Rows': 1,
                    'BgColor' : VIBER_WHITE,
                    'Text' : '<font color="{}">{}</font>'.format(VIBER_DARK_GREY, action_option),
                    'TextSize' : 'large',
                    'ActionType' : 'reply',
                    'ActionBody' : action_option
                }
        buttons.append(button)
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'choose need sites'),
        'keyboard': {
                'DefaultHeight' : False,
                'Buttons' : buttons
            }
    }
    track_data.pop('bot_type', None)
    track_data.update({'current': 'add/del site step2', 'available_sites': bot_type['actions_options']['add/del site bot']})
    await send_message(app, message, track_data)


# add/del site step 2
async def add_del_site_step2(data, app, track_data):
    db = app['db_masterbot']
    if data['message']['text'] == 'add site btn':
        await db.users.update_one({'viber_id': data['sender']['id'], 'bots.bot_uri': track_data['bot_uri']}, {'$addToSet': {'bots.$.sites': {'$each': track_data['sites']}}})
        message = {
                    'receiver': data['sender']['id'],
                    'text': langmes(data['sender']['language'], 'sites added'),
                    'keyboard': {
                                'DefaultHeight' : False,
                                'Buttons' : [
                                    {
                                        'Columns': 6,
                                        'Rows': 1,
                                        'BgColor' : VIBER_YELLOW,
                                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'back to my bots btn')),
                                        'TextSize' : 'large',
                                        'ActionType' : 'reply',
                                        'ActionBody' : SECRET_KEY + 'my bots btn'
                                    }
                                ]
                            }
                }
        track_data.pop('current', None)
    elif data['message']['text'] == 'del site btn':
        await db.users.update_one({'viber_id': data['sender']['id'], 'bots.bot_uri': track_data['bot_uri']}, {'$pull': {'bots.$.sites': {'$in': track_data['sites']}}})
        message = {
                    'receiver': data['sender']['id'],
                    'text': langmes(data['sender']['language'], 'sites deleted'),
                    'keyboard': {
                                'DefaultHeight' : False,
                                'Buttons' : [
                                    {
                                        'Columns': 6,
                                        'Rows': 1,
                                        'BgColor' : VIBER_YELLOW,
                                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'back to my bots btn')),
                                        'TextSize' : 'large',
                                        'ActionType' : 'reply',
                                        'ActionBody' : SECRET_KEY + 'my bots btn'
                                    }
                                ]
                            }
                }
        track_data.pop('current', None)
    else:
        try:
            track_data['available_sites'].remove(data['message']['text'])
            if 'sites' in track_data:
                track_data['sites'].append(data['message']['text'])
            else:
                track_data['sites'] = [data['message']['text']]     
        
            buttons = []
            if len(track_data['available_sites']) > 0:
                for available_site in track_data['available_sites']:
                    button = {
                            'Columns': 6,
                            'Rows': 1,
                            'BgColor' : VIBER_WHITE,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_DARK_GREY, available_site),
                            'TextSize' : 'large',
                            'ActionType' : 'reply',
                            'ActionBody' : available_site
                        }
                    buttons.append(button)
            
            add_btn = {
                        'Columns': 6,
                        'Rows': 1,
                        'BgColor' : VIBER_BLUE,
                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'add site btn')),
                        'TextSize' : 'large',
                        'ActionType' : 'reply',
                        'ActionBody' : 'add site btn'
                    }
            buttons.append(add_btn)

            del_btn = {
                        'Columns': 6,
                        'Rows': 1,
                        'BgColor' : VIBER_RED,
                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE, langmes(data['sender']['language'], 'del site btn')),
                        'TextSize' : 'large',
                        'ActionType' : 'reply',
                        'ActionBody' : 'del site btn'
                    }
            buttons.append(del_btn)

            count_of_sites = len(track_data['sites'])
            message = {
                'receiver': data['sender']['id'],
                'text': langmes(data['sender']['language'], 'count of sites') + ' ' + str(count_of_sites) + '\n' + langmes(data['sender']['language'], 'choose sites again'),
                'keyboard': {
                        'DefaultHeight' : False,
                        'Buttons' : buttons
                    }
            }
        except ValueError:
            pass
        
    if 'message' in locals():
        await send_message(app, message, track_data)