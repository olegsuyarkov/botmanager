from ..constants import BOT_NAME
from ..lang_pack import langmes
from ..sender import send_message
from ...general_constants import VIBER_VIOLET, VIBER_WHITE, SECRET_KEY


async def conversation(data):
    welcome = {
        'sender': {
            'name': BOT_NAME
        },
        'type': 'text',
        'text': langmes(data['user']['language'], 'conversation start'),
        'tracking_data': '{"conversation": "yes"}'
    }
    return welcome


async def subscribe(data, app):
    message = {
            'receiver': data['user']['id'],
            'text': langmes(data['user']['language'], 'first message'),
            'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'BgColor' : VIBER_VIOLET,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['user']['language'], 'send number btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'share-phone',
                            'ActionBody' : SECRET_KEY + 'register number'
                        }
                    ]
                }
        }
    await send_message(app, message)


async def unsubscribe(data, app):
    db = app['db_masterbot']
    await db.users.update_one({'viber_id': data['user_id']}, {'$set':{'status': 'unsubscribed'}})