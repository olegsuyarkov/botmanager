from .handlers_list import handler
from ..general_message_router import message_router as g_router


async def message_router(data, app):
    await g_router(data, app, handler)