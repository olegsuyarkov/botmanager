import json
from ..general_constants import DEFAULT_LANGUAGE, VIBER_GET_ACCOUNT_INFO_ENDPOINT, VIBER_SET_WEBHOOK_ENDPOINT, APP_URL

async def webhook(action, message_body, tools):
    print('task webhook')
    # create url of user bot
    db = tools['db_masterbot']
    message_body = json.loads(message_body.decode())
    user_id = await db.users.find_one_and_update({'viber_id' : message_body['viber_id']}, {'$pull': {'bots': {'bot_uri': message_body['bot_uri']}}})
    user_token_uri = APP_URL + '/cargobot/' + str(user_id['_id']) + '/' + message_body['bot_uri']

    # set webhook request
    headers = {'X-Viber-Auth-Token': message_body['bot_token']}
    if action == 'create' or action == 'activated':
        data = {
                'url': user_token_uri,
                'send_name': 'true'
                }
    elif action == 'deactivated':
        data = {
            'url': ''
        }
    response_webhook = await tools['http'].post(VIBER_SET_WEBHOOK_ENDPOINT, json=data, headers=headers)
    response_webhook = await response_webhook.json()

    # check status of setting webhook
    if response_webhook['status'] == 0 and action == 'create':
        # get bot name from viber
        response_info = await tools['http'].post(VIBER_GET_ACCOUNT_INFO_ENDPOINT, json={}, headers=headers)
        response_info = await response_info.json()

        if response_info['status'] == 0:
            message_body['bot_name'] = response_info['name']

            # get bot system info from db
            system_data_bot = await db.bots_templates.find_one({'name': 'cargobot'}, {'_id': 0, 'options': 1})
            try:
                message_body['system_name'] = system_data_bot['options'][message_body['lang']]['system_name']
            except KeyError:
                message_body['system_name'] = system_data_bot['options'][DEFAULT_LANGUAGE]['system_name']

            message_body.pop('lang')
            message_body.pop('viber_id')
            message_body['status'] = 'activated'

            # save data to db
            await db.users.update_one({'_id': user_id['_id']}, {'$addToSet': {'bots': message_body}})