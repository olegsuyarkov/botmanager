from .constants import BOT_NAME, BOT_TOKEN, BOT_URI_ID
from .. import general_sender

async def send_message(app, message, tracking_data={}, type_mes='text'):
    await general_sender.send_message(app, BOT_TOKEN, BOT_NAME, BOT_URI_ID, message, tracking_data, type_mes)