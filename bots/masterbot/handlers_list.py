from . import registration_handler as rh
from . import menu_handler as mh
from . import bots_item_handler as bih

handler = {
    # word commands handlers
    'Menu': mh.command_menu,
    'Start': rh.command_start,
    # registration handlers
    'register number': rh.register_number,
    # menu items handlers
    'my bots btn': bih.my_bots_btn,
    # create bot btn item
    'create bot btn': bih.create_bot_btn,
    'choose bot template': bih.choose_bot_template,
    'cargobot bot type': bih.cargobot_type_bot,
    'get uri bot': bih.get_uri_bot,
    'get token bot': bih.get_token_bot,
    'save bot info': bih.save_bot_info,
    # actions with bots
    'bots actions': bih.bots_actions,
    'actions selected bot': bih.actions_selected_bot,
    'on/off bot': bih.on_off_bot,
    'on/off bot action': bih.on_off_bot_action,
    'add/del user bot': bih.add_del_user_bot,
    'list of users bot': bih.list_of_users_bot,
    'add/del site bot': bih.add_del_site_bot,
    'add/del site step2': bih.add_del_site_step2
}