from .handlers_list import handler, no_auth_handler
from .helpers import user_cache
from ..general_message_router import message_router as g_router
from ..general_constants import SECRET_KEY


async def message_router(data, request):
    print(data['message']['text'][len(SECRET_KEY):])
    if data['message']['text'].startswith(SECRET_KEY):
        if data['message']['text'][len(SECRET_KEY):] not in no_auth_handler:
            print('no auth + secret')
            is_active, status = await user_cache(data['sender']['id'], request.app, 'is_active', 'status')
            if is_active == None or is_active == 'no' or status == 'unsubscribed':
                data['message']['text'] = SECRET_KEY + 'no_auth'
    elif data['message']['text'] not in no_auth_handler:
        print('no auth')
        is_active, status = await user_cache(data['sender']['id'], request.app, 'is_active', 'status')
        if is_active == None or is_active == 'no' or status == 'unsubscribed':
            data['message']['text'] = SECRET_KEY + 'no_auth'
    await g_router(data, request, handler)