from bson import ObjectId
from ..sender import send_message
from ..lang_pack import langmes
from ...general_constants import VIBER_VIOLET, VIBER_WHITE, SECRET_KEY


# handler by text command "Start"
async def command_start(data, request, track_data):
    if 'conversation' in track_data and track_data['conversation']=='yes':
        message = {
            'receiver': data['sender']['id'],
            'text': langmes(data['sender']['language'], 'first message'),
            'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'BgColor' : VIBER_VIOLET,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'send number btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'share-phone',
                            'ActionBody' : SECRET_KEY + 'register number'
                        }
                    ]
                }
        }
        await send_message(request, message)


# registration phone number of user
async def register_number(data, request, track_data):
    db_masterbot = request.app['db_masterbot']
    user_id =request.match_info['user_id']
    bot_uri = request.match_info['bot_uri']
    contact_flag = False
    boss_data = await db_masterbot.users.find_one({'_id': ObjectId(user_id)})
    for bot in boss_data['bots']:
        if bot['bot_uri'] == bot_uri:
            if 'contacts' in bot:
                for contact in bot['contacts']:
                    if data['message']['contact']['phone_number'] in contact['phone']:
                        contact_flag = True
                        break
            break

    if contact_flag == True:
        db_cargobot = request.app['db_cargobot']
        await request.app['redis'].hmset_dict(data['sender']['id'], {'is_active': 'yes', 'status': 'subscribed'})
        await db_cargobot.users.update_one({'viber_id': data['sender']['id']}, {'$set': {'name': data['sender']['name'], 'language': data['sender']['language'], 'country': data['sender']['country'], 'phone': data['message']['contact']['phone_number'], 'bot_owner_id': request.match_info['user_id'], 'bot_owner_uri': request.match_info['bot_uri'], 'is_active': 'yes', 'status': 'subscribed'}, '$currentDate': {'lastModified': True}}, upsert = True)
        message = {
                'receiver': data['sender']['id'],
                'text': langmes(data['sender']['language'], 'register number already'),
            }
    else:
        message = {
            'receiver': data['sender']['id'],
            'text': langmes(data['sender']['language'], 'register again'),
            'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'BgColor' : VIBER_VIOLET,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'send number btn again')),
                            'TextSize' : 'large',
                            'ActionType' : 'share-phone',
                            'ActionBody' : SECRET_KEY + 'register number'
                        }
                    ]
                }
        }
    await send_message(request, message)


async def no_auth(data, request, track_data):
    message = {
        'receiver': data['sender']['id'],
        'text': langmes(data['sender']['language'], 'register again'),
        'keyboard': {
                'DefaultHeight' : False,
                'Buttons' : [
                    {
                        'BgColor' : VIBER_VIOLET,
                        'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['sender']['language'], 'send number btn')),
                        'TextSize' : 'large',
                        'ActionType' : 'share-phone',
                        'ActionBody' : SECRET_KEY + 'register number'
                    }
                ]
            }
    }
    await send_message(request, message)