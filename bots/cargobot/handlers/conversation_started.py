from ..lang_pack import langmes
from ..helpers import bot_cache
from ..sender import send_message
from ...general_constants import VIBER_VIOLET, VIBER_WHITE, SECRET_KEY


async def conversation(data, request):
    user_id = request.match_info['user_id']
    bot_uri = request.match_info['bot_uri']
    bot_name = await bot_cache(user_id, bot_uri, request.app, 'bot_name')
    welcome = {
        'sender': {
            'name': bot_name[0]
        },
        'type': 'text',
        'text': langmes(data['user']['language'], 'conversation start'),
        'tracking_data': '{"conversation": "yes"}'
    }
    return welcome


async def subscribe(data, request):
    message = {
            'receiver': data['user']['id'],
            'text': langmes(data['user']['language'], 'first message'),
            'keyboard': {
                    'DefaultHeight' : False,
                    'Buttons' : [
                        {
                            'BgColor' : VIBER_VIOLET,
                            'Text' : '<font color="{}">{}</font>'.format(VIBER_WHITE ,langmes(data['user']['language'], 'send number btn')),
                            'TextSize' : 'large',
                            'ActionType' : 'share-phone',
                            'ActionBody' : SECRET_KEY + 'register number'
                        }
                    ]
                }
        }
    await send_message(request, message)


async def unsubscribe(data, request):
    db = request.app['db_cargobot']
    await request.app['redis'].delete(data['user_id'])
    await db.users.update_one({'viber_id': data['user_id']}, {'$set':{'status': 'unsubscribed', 'is_active': 'no'}})