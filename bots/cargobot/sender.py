from .helpers import bot_cache
from .. import general_sender


async def send_message(request, message, tracking_data={}, type_mes='text'):
    bot_uri_id = request.match_info['user_id'] + '_' + request.match_info['bot_uri']
    bot_token, bot_name = await bot_cache(request.match_info['user_id'], request.match_info['bot_uri'], request.app, 'bot_token', 'bot_name')
    await general_sender.send_message(request.app, bot_token, bot_name, bot_uri_id, message, tracking_data, type_mes)