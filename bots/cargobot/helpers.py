from bson.objectid import ObjectId


async def bot_cache(user_id, bot_uri, app, *fields):
    key = user_id + '_' + bot_uri
    cache = await app['redis'].hmget(key, *fields)
    none_counter = 0
    for item in cache:
        if item == None:
            none_counter += 1
            break
    if none_counter > 0:
        from_db = await app['db_masterbot'].users.find_one({'_id': ObjectId(user_id)}, {'bots': 1})
        for bot in from_db['bots']:
            if bot['bot_uri'] == bot_uri:
                await app['redis'].hmset_dict(key, {field: bot[field] for field in fields})
                break
        return [bot[field] for field in fields]
    else:
        return [field.decode() for field in cache]


async def user_cache(viber_id, app, *fields):
    cache = await app['redis'].hmget(viber_id, *fields)
    none_counter = 0
    for item in cache:
        if item == None:
            none_counter += 1
            break
    if none_counter > 0:
        from_db = await app['db_cargobot'].users.find_one({'viber_id': viber_id})
        if from_db == None:
            return [None for field in fields]
        else:
            await app['redis'].hmset_dict(viber_id, {field: from_db[field] for field in fields})
            return [from_db[field] for field in fields]
    else:
        return [field.decode() for field in cache]