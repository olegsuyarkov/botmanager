from . import registration_handler as rh
from . import menu_handler as mh


no_auth_handler = {
    'Start': rh.command_start,
    'register number': rh.register_number
}

handler = {
    # word commands handlers
    'Menu': mh.command_menu,
    'Start': rh.command_start,
    # registration handlers
    'register number': rh.register_number,
    'no_auth': rh.no_auth
}