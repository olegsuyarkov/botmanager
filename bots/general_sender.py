import asyncio
import json
from .general_constants import VIBER_INTERVAL_LIMIT, VIBER_MIN_API_VERSION, VIBER_SEND_MESSAGE_ENDPOINT


async def send_message(app, bot_token, bot_name, bot_uri_id, message, tracking_data, type_mes):
    loop = asyncio.get_running_loop()
    headers = {
        'X-Viber-Auth-Token': bot_token
        }
    track_data = json.dumps(tracking_data)
    message_template = {
        'min_api_version': VIBER_MIN_API_VERSION,
        'sender': {
            'name': bot_name
        },
        'type': type_mes,
        'tracking_data': track_data
    }
    message.update(message_template)

    current_bot_next_call_at = await app['redis'].hmget(bot_uri_id, 'viber_next_call')
    if current_bot_next_call_at[0] == None:
        viber_next_call_at = 0.0
    else:
        viber_next_call_at = float(current_bot_next_call_at[0])

    if loop.time() < viber_next_call_at:
        next_call = viber_next_call_at - loop.time()
        redis_next_call = viber_next_call_at + VIBER_INTERVAL_LIMIT
        await app['redis'].hmset_dict(bot_uri_id, {'viber_next_call': redis_next_call})
        await asyncio.sleep(next_call)
        response = await app['viber_session'].post(VIBER_SEND_MESSAGE_ENDPOINT, json=message, headers=headers)
        # debug output data
        if app['debug_mode'] == 'true':
            print(await response.json())
    else: 
        redis_next_call = loop.time() + VIBER_INTERVAL_LIMIT
        await app['redis'].hmset_dict(bot_uri_id, {'viber_next_call': redis_next_call})
        response = await app['viber_session'].post(VIBER_SEND_MESSAGE_ENDPOINT, json=message, headers=headers)
        # debug output data
        if app['debug_mode'] == 'true':
            print(await response.json())