import json
from .general_constants import SECRET_KEY


async def message_router(data, app, handler):
    if (data['message']['type'] == 'text') or (data['message']['type'] == 'contact'):
        text = data['message']['text']
        if 'tracking_data' in data['message']:
            tracking_data = json.loads(data['message']['tracking_data'])
            if text == 'btn_back':
                try:
                    await handler[tracking_data['prev']](data, app, tracking_data)
                except KeyError:
                    pass
            elif text == 'Menu':
                await handler['Menu'](data, app, tracking_data)
            elif text =='Start':
                await handler['Start'](data, app, tracking_data)
            elif 'current' in tracking_data:
                await handler[tracking_data['current']](data, app, tracking_data)
            elif text.startswith(SECRET_KEY):
                # try:
                await handler[text[len(SECRET_KEY):]](data, app, tracking_data)
                # except KeyError:
                #     if app['debug_mode'] == 'true':
                #         print('KeyError')
                    # else:
                    #     pass
            else:
                pass