import os
import json
import asyncio
from datetime import datetime
import uvloop
import aioredis
import motor.motor_asyncio
from aiohttp import web, ClientSession
from aio_pika import connect, ExchangeType, Message, DeliveryMode
# ***bot's packages***
# masterbot
from bots.masterbot.handlers.conversation_started import conversation as masterbot_conversation
from bots.masterbot.handlers.conversation_started import subscribe as masterbot_subscribe
from bots.masterbot.handlers.conversation_started import unsubscribe as masterbot_unsubscribe
from bots.masterbot.message_router import message_router as masterbot_message_router
# cargobot
from bots.cargobot.handlers.conversation_started import conversation as cargobot_conversation
from bots.cargobot.handlers.conversation_started import subscribe as cargobot_subscribe
from bots.cargobot.handlers.conversation_started import unsubscribe as cargobot_unsubscribe
from bots.cargobot.message_router import message_router as cargobot_message_router


# set event loop
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


# set Application
app = web.Application()


# set debug mode flag
app['debug_mode'] = os.getenv('AIOHTTP_DEBUG')


# STARTUP AND CLEANUP
# startup
async def create_httpclient(app):
    app['viber_session'] = ClientSession()
    print('* start http_client')

async def create_redis(app):
    app['redis'] = await aioredis.create_redis_pool(('redis', 6379))
    app['redis'].execute('FLUSHALL')
    print('* start redis')

async def create_motor(app):
    connection_string = 'mongodb://{}:{}@mongodb:27017'.format(os.getenv('MONGO_INITDB_ROOT_USERNAME'), os.getenv('MONGO_INITDB_ROOT_PASSWORD'))
    app['motor'] = motor.motor_asyncio.AsyncIOMotorClient(connection_string)
    app['db_masterbot'] = app['motor'].masterbot
    app['db_cargobot'] = app['motor'].cargobot
    print('* start motor')
    # СОЗДАТЬ ИНИЦИАЛИЗАЦИЮ НЕКОТОРЫХ ДАННЫХ БАЗЫ: СПИСОК ШАБЛОНОВ БОТОВ И Т.П.

async def create_rabbitmq(app):
    app['rabbit_connection'] = await connect("amqp://{}:{}@rabbitmq/".format(os.getenv('RABBITMQ_DEFAULT_USER'), os.getenv('RABBITMQ_DEFAULT_PASS')), loop=asyncio.get_event_loop())
    channel = await app['rabbit_connection'].channel()
    # masterbot exchange
    app['masterbot_exchange'] = await channel.declare_exchange('masterbot', ExchangeType.TOPIC)
    print('* start rabbitmq')

app.on_startup.append(create_httpclient)
app.on_startup.append(create_redis)
app.on_startup.append(create_motor)
app.on_startup.append(create_rabbitmq)


# cleanup
async def close_httpclient(app):
    await app['viber_session'].close()
    print('** stop http_client')

async def close_redis(app):
    app['redis'].close()
    await app['redis'].wait_closed()
    print('** stop redis')

async def close_motor(app):
    app['motor'].close()
    print('** stop motor')

async def close_rabbitmq(app):
    await app['rabbit_connection'].close()
    print('** stop rabbit')

app.on_cleanup.append(close_httpclient)
app.on_cleanup.append(close_redis)
app.on_cleanup.append(close_motor)
app.on_cleanup.append(close_rabbitmq)


# HANDLERS
# check system working
async def check_system(request):
    message = {
        'status': 'ok',
        'message': 'Service is working!',
        'time': datetime.now().strftime('%H:%M')
    }
    return web.Response(text=json.dumps(message))


# handler for different testing
async def tests(request):
    message_body = {'viber_id': 'Y6cpkLRBgHrkuiojkuwpEg=='}
    message_body = Message(json.dumps(message_body).encode(), delivery_mode=DeliveryMode.PERSISTENT)
    await app['masterbot_exchange'].publish(message_body, routing_key='webhook.delete')
    # await app['redis'].hmset_dict('master_bot', {'viber_next_call': '0.1'})
    # redis_key = await app['redis'].hmget('master_bot', 'viber_next_call')
    # print(float(redis_key[0]))
    return web.Response(status=200, text='OK')


# master bot viber handler webhook
async def masterbot_handler(request):
    data = await request.json()
    if data['event'] == 'conversation_started':
        message = await masterbot_conversation(data)
        resp = web.Response(status=200, text=json.dumps(message))
    elif data['event'] == 'message':
        await masterbot_message_router(data, request.app)
        resp = web.Response(status=200)
    elif data['event'] == 'subscribed':
        await masterbot_subscribe(data, request.app)
        resp = web.Response(status=200)
    elif data['event'] == 'unsubscribed':
        await masterbot_unsubscribe(data, request.app)
        resp = web.Response(status=200)
    else:
        resp = web.Response(status=200)

    #debug input data from viber
    if app['debug_mode'] == 'true':
        print(data)
    
    return resp


# cargobot viber handler webhook
async def cargobot_handler(request):
    data = await request.json()
    if data['event'] == 'conversation_started':
        message = await cargobot_conversation(data, request)
        resp = web.Response(status=200, text=json.dumps(message))
    elif data['event'] == 'message':
        await cargobot_message_router(data, request)
        resp = web.Response(status=200)
    elif data['event'] == 'subscribed':
        await cargobot_subscribe(data, request)
        resp = web.Response(status=200)
    elif data['event'] == 'unsubscribed':
        await cargobot_unsubscribe(data, request)
        resp = web.Response(status=200)
    else:
        resp = web.Response(status=200)

    #debug input data from viber
    if app['debug_mode'] == 'true':
        print(data)
    
    return resp


# ROUTES
app.add_routes([web.get('/check', check_system),
                web.get('/test', tests),
                web.post('/masterbot_handler', masterbot_handler),
                web.post('/cargobot/{user_id}/{bot_uri}', cargobot_handler)
                ])


# DEBUG MODE ENABLED
if app['debug_mode'] == 'true':
    import aiohttp_debugtoolbar
    aiohttp_debugtoolbar.setup(app, intercept_redirects=False)


# RUN APP
if __name__ == '__main__':
    web.run_app(app)