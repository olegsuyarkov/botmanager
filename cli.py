import os
import click
import requests
# import json
from cryptography.fernet import Fernet
from bots.general_constants import APP_URL


@click.group()
def cli():
    """Select command from following list"""
    pass


@cli.command()
def test():
    """Test command"""
    click.echo(APP_URL)
    # click.echo('Test echo!')
    # test_dict = {
    #     'current_action': 'first_message',
    #     'prev_action': 'none'
    # }
    # test_dict = None
    # click.echo(json.dumps(test_dict))

@cli.command()
def fernet():
    """Generate Fernet key and save it in file"""

    key = Fernet.generate_key()
    file = open('fernet.key', 'wb')
    file.write(key) # The key is type bytes still
    file.close()


@cli.command()
@click.option('--string', help='Take a string, that need to be hashed', required=True)
def hash_string(string):
    """Hash input string"""
    file = open('fernet.key', 'rb')
    fernet_key = file.read() # The key will be type bytes
    file.close()
    message = string.encode()
    f = Fernet(fernet_key)
    encrypted = f.encrypt(message)
    click.echo(encrypted)


@cli.command()
@click.option('--token_uri', help='Set token uri of bot', required=True, type=click.STRING)
@click.option('--token', help='Set token of bot', required=True, type=click.STRING)
def webhook(token_uri, token):
    """Settings for viber bot"""

    url = 'https://chatapi.viber.com/pa/set_webhook'
    try:
        if token == 'master':
            viber_token = os.getenv('MASTERBOT_TOKEN')
        else:
            viber_token = token

        if token_uri == 'master':
            data = {
                'url': APP_URL + '/masterbot_handler',
                'send_name': 'true'
            }
        elif token_uri == 'none':
            data = {
                'url': ''
            }
        else:
            data = {
                'url': APP_URL + '/' + token_uri,
                'send_name': 'true'
            }


        headers = {'X-Viber-Auth-Token': viber_token}

        r = requests.post(url, headers=headers, json=data)
        click.echo('status code: {}'.format(r.status_code))
        click.echo('headers: {}'.format(r.headers))
        click.echo('data: {}'.format(r.json()))
    except KeyError:
        click.echo("You didn't set token or token uri!")


if __name__ == '__main__':
    cli()